var class_premier_league_1_1_data_1_1_matches =
[
    [ "AwayTeam", "class_premier_league_1_1_data_1_1_matches.html#ad5ffb9551682dd47752fd952f570361b", null ],
    [ "AwayTeamGoals", "class_premier_league_1_1_data_1_1_matches.html#adefd97a705fb860b024a829ee34d46b1", null ],
    [ "Date", "class_premier_league_1_1_data_1_1_matches.html#af008526645664c5e9cea9d197c560879", null ],
    [ "HomeTeam", "class_premier_league_1_1_data_1_1_matches.html#a10febfdd118f3df75c12fae25cce2560", null ],
    [ "HomeTeamGoals", "class_premier_league_1_1_data_1_1_matches.html#a40e7636a36b772ed0a20b9da11879566", null ],
    [ "MatchID", "class_premier_league_1_1_data_1_1_matches.html#aa1c71eac31f2542c9b87e0eb50d29e3c", null ],
    [ "Referee", "class_premier_league_1_1_data_1_1_matches.html#a813c01ca9797e6bd150a6422b31be3c3", null ],
    [ "Result", "class_premier_league_1_1_data_1_1_matches.html#ae8945a5b8f1d2436714fd8cdffe76657", null ],
    [ "Teams", "class_premier_league_1_1_data_1_1_matches.html#a37676ba5f9f3712d50dedba7b9c6d465", null ],
    [ "Teams1", "class_premier_league_1_1_data_1_1_matches.html#a0cc4b081a1646500107b179e6a6fdd90", null ]
];