var namespace_premier_league_1_1_repository =
[
    [ "IRepository", "interface_premier_league_1_1_repository_1_1_i_repository.html", "interface_premier_league_1_1_repository_1_1_i_repository" ],
    [ "MatchesRepository", "class_premier_league_1_1_repository_1_1_matches_repository.html", "class_premier_league_1_1_repository_1_1_matches_repository" ],
    [ "PlayersRepository", "class_premier_league_1_1_repository_1_1_players_repository.html", "class_premier_league_1_1_repository_1_1_players_repository" ],
    [ "RepositoryContainer", "class_premier_league_1_1_repository_1_1_repository_container.html", "class_premier_league_1_1_repository_1_1_repository_container" ],
    [ "TeamsRepository", "class_premier_league_1_1_repository_1_1_teams_repository.html", "class_premier_league_1_1_repository_1_1_teams_repository" ]
];