var class_premier_league_1_1_repository_1_1_players_repository =
[
    [ "Create", "class_premier_league_1_1_repository_1_1_players_repository.html#af7ec52fc5cff3b45f7b46f7807b7662e", null ],
    [ "Delete", "class_premier_league_1_1_repository_1_1_players_repository.html#a3e7de1ef086d8b2b160d776099ebb9bb", null ],
    [ "Read", "class_premier_league_1_1_repository_1_1_players_repository.html#a1314a8b2042e0d6333b8e6e2da37c60e", null ],
    [ "Update", "class_premier_league_1_1_repository_1_1_players_repository.html#a9e858ee7d977a2f944d1ef5185c987c6", null ],
    [ "Parent", "class_premier_league_1_1_repository_1_1_players_repository.html#a23c88e61207b48e00b2670c467e25f01", null ],
    [ "PlayersTable", "class_premier_league_1_1_repository_1_1_players_repository.html#a6ec8fb51f81b6f3799a05aa083b0d134", null ]
];