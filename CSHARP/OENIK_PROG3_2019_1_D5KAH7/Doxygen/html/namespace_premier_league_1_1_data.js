var namespace_premier_league_1_1_data =
[
    [ "Matches", "class_premier_league_1_1_data_1_1_matches.html", "class_premier_league_1_1_data_1_1_matches" ],
    [ "Players", "class_premier_league_1_1_data_1_1_players.html", "class_premier_league_1_1_data_1_1_players" ],
    [ "PremierLeagueDataModel", "class_premier_league_1_1_data_1_1_premier_league_data_model.html", "class_premier_league_1_1_data_1_1_premier_league_data_model" ],
    [ "Teams", "class_premier_league_1_1_data_1_1_teams.html", "class_premier_league_1_1_data_1_1_teams" ]
];