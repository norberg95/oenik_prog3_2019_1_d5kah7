var class_premier_league_1_1_data_1_1_teams =
[
    [ "Teams", "class_premier_league_1_1_data_1_1_teams.html#a325cd431203bf0038de22cef7cdc1805", null ],
    [ "Captain", "class_premier_league_1_1_data_1_1_teams.html#ac238d3ef86e17c90ebd9ec678210f558", null ],
    [ "KitSponsor", "class_premier_league_1_1_data_1_1_teams.html#a7ea7b1334bc25d70059beca07eeaff70", null ],
    [ "Manager", "class_premier_league_1_1_data_1_1_teams.html#a04147d8cc4fdd2180b2a8eecb1c73da4", null ],
    [ "Matches", "class_premier_league_1_1_data_1_1_teams.html#a2f7860bb95a6ff76c5d7ed15ec44afac", null ],
    [ "Matches1", "class_premier_league_1_1_data_1_1_teams.html#ab61556d4b0051db0ba79aa0123e6e30b", null ],
    [ "Players", "class_premier_league_1_1_data_1_1_teams.html#a801ec77f018456115899af460327678f", null ],
    [ "Team", "class_premier_league_1_1_data_1_1_teams.html#a7a6d66b7e743d67e1a687ff2b44b8878", null ]
];