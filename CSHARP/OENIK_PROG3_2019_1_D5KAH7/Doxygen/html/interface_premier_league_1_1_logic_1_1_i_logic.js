var interface_premier_league_1_1_logic_1_1_i_logic =
[
    [ "AverageAgeByTeam", "interface_premier_league_1_1_logic_1_1_i_logic.html#ac48f9b2e20348250f18027c956556b9d", null ],
    [ "CreateMatch", "interface_premier_league_1_1_logic_1_1_i_logic.html#a58db1091e9ace25e74e7349009132a5b", null ],
    [ "CreatePlayer", "interface_premier_league_1_1_logic_1_1_i_logic.html#ab01710fc26a49c68fb794b642e4efd3a", null ],
    [ "CreateTeam", "interface_premier_league_1_1_logic_1_1_i_logic.html#aece63ca128db4529877dab6fe8ce8246", null ],
    [ "DeleteMatch", "interface_premier_league_1_1_logic_1_1_i_logic.html#a0eedab3aa5f8305caebd724d315f2f23", null ],
    [ "DeletePlayer", "interface_premier_league_1_1_logic_1_1_i_logic.html#a6b7be2c955a86fe4ba1351156ad550ac", null ],
    [ "DeleteTeam", "interface_premier_league_1_1_logic_1_1_i_logic.html#a44ae636194b8afbb56a9993b2b4b0590", null ],
    [ "LoadFromXML", "interface_premier_league_1_1_logic_1_1_i_logic.html#a5da64d0d5727f6975df8a625dffa76de", null ],
    [ "NumberOfMatchesByReferee", "interface_premier_league_1_1_logic_1_1_i_logic.html#af52c73ece53694749b9bbae8ad2e7bb5", null ],
    [ "ReadMatchesTable", "interface_premier_league_1_1_logic_1_1_i_logic.html#a08cb1b295bc1d272a66b4abe1154560b", null ],
    [ "ReadPlayerTable", "interface_premier_league_1_1_logic_1_1_i_logic.html#a2784434ced62d07dc18ef1fcccc820af", null ],
    [ "ReadTeamsTable", "interface_premier_league_1_1_logic_1_1_i_logic.html#a07ba99b2a3cc29f6be251f7e517c5915", null ],
    [ "TeamsBySponsor", "interface_premier_league_1_1_logic_1_1_i_logic.html#afbcd2fdb857ba3bb4885849fddb164db", null ],
    [ "UpdateMatch", "interface_premier_league_1_1_logic_1_1_i_logic.html#a1d16e8cb555f4cd160adf9c0aa851b5f", null ],
    [ "UpdatePlayer", "interface_premier_league_1_1_logic_1_1_i_logic.html#a679592e19bd7eb6f83150f3b38c75f3e", null ],
    [ "UpdateTeam", "interface_premier_league_1_1_logic_1_1_i_logic.html#a3f3f04a5f22ee9f16b180451d90201d6", null ]
];