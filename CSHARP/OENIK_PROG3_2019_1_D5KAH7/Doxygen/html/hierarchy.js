var hierarchy =
[
    [ "PremierLeague.Logic.Tests.BLTests", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html", null ],
    [ "OENIK_PROG3_2019_1_D5KAH7.ConsoleTable", "class_o_e_n_i_k___p_r_o_g3__2019__1___d5_k_a_h7_1_1_console_table.html", null ],
    [ "OENIK_PROG3_2019_1_D5KAH7.ConsoleTableOptions", "class_o_e_n_i_k___p_r_o_g3__2019__1___d5_k_a_h7_1_1_console_table_options.html", null ],
    [ "DbContext", null, [
      [ "PremierLeague.Data.PremierLeagueDataModel", "class_premier_league_1_1_data_1_1_premier_league_data_model.html", null ]
    ] ],
    [ "PremierLeague.Logic.ILogic", "interface_premier_league_1_1_logic_1_1_i_logic.html", [
      [ "PremierLeague.Logic.BL", "class_premier_league_1_1_logic_1_1_b_l.html", null ]
    ] ],
    [ "PremierLeague.Repository.IRepository< T >", "interface_premier_league_1_1_repository_1_1_i_repository.html", null ],
    [ "PremierLeague.Repository.IRepository< Matches >", "interface_premier_league_1_1_repository_1_1_i_repository.html", [
      [ "PremierLeague.Repository.MatchesRepository", "class_premier_league_1_1_repository_1_1_matches_repository.html", null ]
    ] ],
    [ "PremierLeague.Repository.IRepository< Players >", "interface_premier_league_1_1_repository_1_1_i_repository.html", [
      [ "PremierLeague.Repository.PlayersRepository", "class_premier_league_1_1_repository_1_1_players_repository.html", null ]
    ] ],
    [ "PremierLeague.Repository.IRepository< Teams >", "interface_premier_league_1_1_repository_1_1_i_repository.html", [
      [ "PremierLeague.Repository.TeamsRepository", "class_premier_league_1_1_repository_1_1_teams_repository.html", null ]
    ] ],
    [ "PremierLeague.Data.Matches", "class_premier_league_1_1_data_1_1_matches.html", null ],
    [ "OENIK_PROG3_2019_1_D5KAH7.Menu", "class_o_e_n_i_k___p_r_o_g3__2019__1___d5_k_a_h7_1_1_menu.html", null ],
    [ "PremierLeague.Data.Players", "class_premier_league_1_1_data_1_1_players.html", null ],
    [ "OENIK_PROG3_2019_1_D5KAH7.Program", "class_o_e_n_i_k___p_r_o_g3__2019__1___d5_k_a_h7_1_1_program.html", null ],
    [ "PremierLeague.Repository.RepositoryContainer", "class_premier_league_1_1_repository_1_1_repository_container.html", null ],
    [ "PremierLeague.Data.Teams", "class_premier_league_1_1_data_1_1_teams.html", null ]
];