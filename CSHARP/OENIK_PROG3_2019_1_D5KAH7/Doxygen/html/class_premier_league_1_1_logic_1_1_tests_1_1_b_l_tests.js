var class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests =
[
    [ "AverageAgeByTeamTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#a8a15412fa49ae878a2204c3418f10e4d", null ],
    [ "MatchesCreateTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#af1d3f3cfe62c1416fb581f8decbf89d3", null ],
    [ "MatchesDeleteTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#a4282180afcc126dedb0f143c71f558ec", null ],
    [ "MatchesReadTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#a8ef3aa1ba8b4d641e961de4b810c0358", null ],
    [ "MatchesUpdateTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#a2b97d1eaa43385b8b59b2c391b7c245f", null ],
    [ "NumberOfMatchesByRefereeTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#a4058c44d747aa2efcfd7e25a7a486171", null ],
    [ "PlayersCreateTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#af8215713cfb582f4c83b62d258836231", null ],
    [ "PlayersDeleteTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#aea768713024e8c8f386bc1f4b2fda7a9", null ],
    [ "PlayersReadTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#aa7246f992d8cf35b2924a92caa6a4f71", null ],
    [ "PlayersUpdateTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#a673f7b8c1b53176089f09b8ef06e9e73", null ],
    [ "SetUp", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#abd295752257dd12609dfd2ac144308a2", null ],
    [ "TeamsBySponsorTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#aa2553e500e02363b5988b14734e03df3", null ],
    [ "TeamsCreateTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#a790c8e9e785b88df810bc58069aae770", null ],
    [ "TeamsDeleteTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#a37c9cab24130f91a469fcfec26b52126", null ],
    [ "TeamsReadTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#ade98ab72f1ff57c335c7dbca5c19c9e1", null ],
    [ "TeamsUpdateTest", "class_premier_league_1_1_logic_1_1_tests_1_1_b_l_tests.html#a7cf468cb3561f77f99337cbba8d0e8e5", null ]
];