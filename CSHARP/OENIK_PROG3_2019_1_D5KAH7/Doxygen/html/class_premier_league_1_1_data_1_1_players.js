var class_premier_league_1_1_data_1_1_players =
[
    [ "Age", "class_premier_league_1_1_data_1_1_players.html#a28b32f51d849c290f84eaba9ab79812c", null ],
    [ "Club", "class_premier_league_1_1_data_1_1_players.html#a02800e3b3de0b0f0d326b33ccc6841f1", null ],
    [ "ClubID", "class_premier_league_1_1_data_1_1_players.html#ac9acc42f476c89b467887504af9578fa", null ],
    [ "Name", "class_premier_league_1_1_data_1_1_players.html#ab0c0a9805c914387ae099138ca934a42", null ],
    [ "Nationality", "class_premier_league_1_1_data_1_1_players.html#a88eb0d5c564e54044f8f76240e076663", null ],
    [ "Position", "class_premier_league_1_1_data_1_1_players.html#af59eeb143b559adab21e34dd6b9c394d", null ],
    [ "Teams", "class_premier_league_1_1_data_1_1_players.html#ab19a2e7c69991a3011ac4144a088bfd2", null ]
];