var class_premier_league_1_1_logic_1_1_b_l =
[
    [ "BL", "class_premier_league_1_1_logic_1_1_b_l.html#a687f47f2ecd79d797436a5488dcda522", null ],
    [ "BL", "class_premier_league_1_1_logic_1_1_b_l.html#a450f5e4f5e4583c225114ce0b6940cad", null ],
    [ "AverageAgeByTeam", "class_premier_league_1_1_logic_1_1_b_l.html#a7445cffb4a7a875f4d7f956e6a12c992", null ],
    [ "CreateMatch", "class_premier_league_1_1_logic_1_1_b_l.html#a2168e6ae23a7131d2f6179b2a7b1a76b", null ],
    [ "CreatePlayer", "class_premier_league_1_1_logic_1_1_b_l.html#ab671c6e23993f6b5386f7dc7574f8e89", null ],
    [ "CreateTeam", "class_premier_league_1_1_logic_1_1_b_l.html#abd2c2d7f554c89c28e3d884b977db4e3", null ],
    [ "DeleteMatch", "class_premier_league_1_1_logic_1_1_b_l.html#a4090f279fb3211c75bf48a8b9d97eeb7", null ],
    [ "DeletePlayer", "class_premier_league_1_1_logic_1_1_b_l.html#ac1cd0d0ce07fd82ed6a4497cfe060bcb", null ],
    [ "DeleteTeam", "class_premier_league_1_1_logic_1_1_b_l.html#a3bf018aea4283debacc5b30ddb90ddcd", null ],
    [ "LoadFromXML", "class_premier_league_1_1_logic_1_1_b_l.html#aeeb635229ae73f0854e81c60e0942dba", null ],
    [ "NumberOfMatchesByReferee", "class_premier_league_1_1_logic_1_1_b_l.html#ab7aa91bb906a103062312a29799969b1", null ],
    [ "ReadMatchesTable", "class_premier_league_1_1_logic_1_1_b_l.html#a9c03ae432262f259cc0d746c9ea99dc8", null ],
    [ "ReadPlayerTable", "class_premier_league_1_1_logic_1_1_b_l.html#ad337fa04801535a504050e5b2d4dc94b", null ],
    [ "ReadTeamsTable", "class_premier_league_1_1_logic_1_1_b_l.html#a609c85135ae63bfea427ff6483e38a41", null ],
    [ "TeamsBySponsor", "class_premier_league_1_1_logic_1_1_b_l.html#a7199fe1f1f8baec523f07ca35a313a73", null ],
    [ "UpdateMatch", "class_premier_league_1_1_logic_1_1_b_l.html#a4969a71bd36f9e0f5058c64e2ac5eb81", null ],
    [ "UpdatePlayer", "class_premier_league_1_1_logic_1_1_b_l.html#ae3877b56afbbaaa59a6c0f592d95ff1b", null ],
    [ "UpdateTeam", "class_premier_league_1_1_logic_1_1_b_l.html#a839e08e40f86b29fa774b7c4538e8d56", null ],
    [ "Repo", "class_premier_league_1_1_logic_1_1_b_l.html#a97785a27f8ee2b6fcf211928b4f88833", null ]
];