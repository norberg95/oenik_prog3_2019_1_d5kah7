var class_premier_league_1_1_repository_1_1_teams_repository =
[
    [ "Create", "class_premier_league_1_1_repository_1_1_teams_repository.html#ae791be6e0b6ff0d56b220abe58f27204", null ],
    [ "Delete", "class_premier_league_1_1_repository_1_1_teams_repository.html#a4a9ff516eb378cc584a71b858956bcaa", null ],
    [ "Read", "class_premier_league_1_1_repository_1_1_teams_repository.html#a902d74eac43e9539af4b06a3c0020a56", null ],
    [ "Update", "class_premier_league_1_1_repository_1_1_teams_repository.html#afad47ce48bd265dc9a197d08cec3dad4", null ],
    [ "Parent", "class_premier_league_1_1_repository_1_1_teams_repository.html#ae6c0d2d978b9c6e865d6b9f55a14ea25", null ],
    [ "TeamsTable", "class_premier_league_1_1_repository_1_1_teams_repository.html#a422b42f354d60a35a497fca500e89b64", null ]
];