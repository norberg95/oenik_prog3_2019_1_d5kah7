var indexSectionsWithContent =
{
  0: "abcdefilmnoprstuw",
  1: "bcimprt",
  2: "op",
  3: "abcdflmnprstuw",
  4: "f",
  5: "adm",
  6: "cemoprt",
  7: "cn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties",
  7: "Pages"
};

