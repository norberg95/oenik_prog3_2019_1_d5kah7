var searchData=
[
  ['parent',['Parent',['../class_premier_league_1_1_repository_1_1_matches_repository.html#aca21c2445394938e63323fb3dfcf4fbb',1,'PremierLeague.Repository.MatchesRepository.Parent()'],['../class_premier_league_1_1_repository_1_1_players_repository.html#a23c88e61207b48e00b2670c467e25f01',1,'PremierLeague.Repository.PlayersRepository.Parent()'],['../class_premier_league_1_1_repository_1_1_teams_repository.html#ae6c0d2d978b9c6e865d6b9f55a14ea25',1,'PremierLeague.Repository.TeamsRepository.Parent()']]],
  ['playersrepo',['PlayersRepo',['../class_premier_league_1_1_repository_1_1_repository_container.html#aba1e9fd0b9c244def9fad674f0c87454',1,'PremierLeague::Repository::RepositoryContainer']]],
  ['playerstable',['PlayersTable',['../class_premier_league_1_1_repository_1_1_players_repository.html#a6ec8fb51f81b6f3799a05aa083b0d134',1,'PremierLeague::Repository::PlayersRepository']]],
  ['premierleaguedatamodel',['PremierLeagueDataModel',['../class_premier_league_1_1_repository_1_1_repository_container.html#ab6f5c9635ecc92e5dbf1997f57f71a62',1,'PremierLeague::Repository::RepositoryContainer']]]
];
