var class_premier_league_1_1_repository_1_1_repository_container =
[
    [ "RepositoryContainer", "class_premier_league_1_1_repository_1_1_repository_container.html#afafee5ab2a9df8e127c3a7ce463c973c", null ],
    [ "RepositoryContainer", "class_premier_league_1_1_repository_1_1_repository_container.html#a02fbad3d43997cde2da984ede5291ed4", null ],
    [ "MatchesRepo", "class_premier_league_1_1_repository_1_1_repository_container.html#a1dde05f037384a2705be4d32cdc30e9e", null ],
    [ "PlayersRepo", "class_premier_league_1_1_repository_1_1_repository_container.html#aba1e9fd0b9c244def9fad674f0c87454", null ],
    [ "PremierLeagueDataModel", "class_premier_league_1_1_repository_1_1_repository_container.html#ab6f5c9635ecc92e5dbf1997f57f71a62", null ],
    [ "TeamsRepo", "class_premier_league_1_1_repository_1_1_repository_container.html#a2f4e8b4ec17451a64693cb03de526277", null ]
];