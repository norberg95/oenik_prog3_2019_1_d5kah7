var class_premier_league_1_1_data_1_1_premier_league_data_model =
[
    [ "PremierLeagueDataModel", "class_premier_league_1_1_data_1_1_premier_league_data_model.html#a2d3012199efc3677d777f2f6e3dc105a", null ],
    [ "OnModelCreating", "class_premier_league_1_1_data_1_1_premier_league_data_model.html#a4a686717f9ae872b33a23401617e4855", null ],
    [ "Matches", "class_premier_league_1_1_data_1_1_premier_league_data_model.html#a14e395a6758779d7091807b1b46d167f", null ],
    [ "Players", "class_premier_league_1_1_data_1_1_premier_league_data_model.html#a36b5a43cd78b52cc7711289645e8c279", null ],
    [ "Teams", "class_premier_league_1_1_data_1_1_premier_league_data_model.html#a63bf94dea0769054fe99e1cc0b3f7f7e", null ]
];