﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace OENIK_PROG3_2019_1_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PremierLeague.Logic;

    /// <summary>
    /// Auto generated Program class
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            BL bl = new BL();
            Menu mn = new Menu(bl);
            mn.MainMenu();
        }
    }
}
