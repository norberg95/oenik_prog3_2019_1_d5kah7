﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace OENIK_PROG3_2019_1_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PremierLeague.Data;
    using PremierLeague.Logic;

    /// <summary>
    /// Menu
    /// </summary>
    internal class Menu
    {
        private readonly BL bl;
        private readonly bool isProgramRunning = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        /// <param name="bl">logic</param>
        public Menu(BL bl)
        {
            this.bl = bl;
        }

        /// <summary>
        /// MainMenu
        /// </summary>
        public void MainMenu()
        {
            ConsoleKeyInfo cki;
            while (this.isProgramRunning)
            {
                char menuSelection;
                char crudSelection;

                Console.Clear();
                Console.WriteLine("English Premier League statictics");
                Console.WriteLine("Choose from the following options");
                Console.WriteLine("1. Matches table");
                Console.WriteLine("2. Players table");
                Console.WriteLine("3. Teams table");
                Console.WriteLine("4. non CRUD - Number of games by a referee");
                Console.WriteLine("5. non CRUD - Average Age by Team");
                Console.WriteLine("6. non CRUD- Teams sponsored by a brand");
                Console.WriteLine("7. Java endpoint");
                Console.WriteLine("8. Exit");
                cki = Console.ReadKey();
                menuSelection = cki.KeyChar;
                Console.Clear();
                switch (menuSelection)
                {
                    case '1':
                        crudSelection = this.CRUDMenu();
                        switch (crudSelection)
                        {
                            case '1':
                                this.ReadMatches();

                                break;
                            case '2':
                                this.CreateMatches();
                                break;
                            case '3':
                                this.UpdateMatches();
                                break;
                            case '4':
                                this.DeleteMatches();
                                break;

                            default:
                                break;
                        }

                        break;
                    case '2':
                        crudSelection = this.CRUDMenu();
                        switch (crudSelection)
                        {
                            case '1':
                                this.ReadPlayers();
                                break;
                            case '2':
                                this.CreatePlayers();
                                break;
                            case '3':
                                this.UpdatePlayers();
                                break;
                            case '4':
                                this.DeletePlayers();
                                break;

                            default:
                                break;
                        }

                        break;
                    case '3':
                        crudSelection = this.CRUDMenu();
                        switch (crudSelection)
                        {
                            case '1':
                                this.ReadTeams();
                                break;
                            case '2':
                                this.CreateTeam();
                                break;
                            case '3':
                                this.UpdateTeam();
                                break;
                            case '4':
                                this.DeleteTeams();
                                break;

                            default:
                                break;
                        }

                        break;
                    case '4':
                        this.NumofGamesByRef();

                        break;
                    case '5':
                        this.AverageAgeByTeam();
                        break;
                    case '6':
                        this.ListTeamsBySponsor();
                        break;
                    case '7':
                        this.LoadFromXML();
                        break;
                    case '8':
                        System.Environment.Exit(0);
                        break;
                    default:
                        continue;
                }

                Console.ReadKey();
            }

            Console.ReadKey();
        }

        private char CRUDMenu()
        {
            Console.WriteLine("1. Read");
            Console.WriteLine("2. Create");
            Console.WriteLine("3. Update");
            Console.WriteLine("4. Delete");

            ConsoleKeyInfo cki = Console.ReadKey();

            Console.Clear();
            return cki.KeyChar;
        }

        private void ReadMatches()
        {
            var table = new ConsoleTable(this.MatchesColumns());
            foreach (var match in this.bl.ReadMatchesTable())
            {
                table.AddRow(match.MatchID, match.Date, match.HomeTeam, match.AwayTeam, match.HomeTeamGoals, match.AwayTeamGoals, match.Result, match.Referee);
            }

            table.Write();
        }

        private void ReadPlayers()
        {
            var table = new ConsoleTable(this.PlayersColumns());
            foreach (var player in this.bl.ReadPlayerTable())
            {
                table.AddRow(player.ClubID, player.Name, player.Club, player.Age, player.Position, player.Nationality);
            }

            table.Write();
        }

        private void ReadTeams()
        {
            var table = new ConsoleTable(this.TeamsColumns());
            foreach (var team in this.bl.ReadTeamsTable())
            {
                table.AddRow(team.Team, team.Manager, team.Captain, team.KitSponsor);
            }

            table.Write();
        }

        private void CreateMatches()
        {
            Console.WriteLine("Date DD/MM/YYY format:");
            string date = Console.ReadLine();
            Console.WriteLine("HomeTeam:");
            string homeTeam = Console.ReadLine();
            Console.WriteLine("AwayTeam:");
            string awayTeam = Console.ReadLine();
            Console.WriteLine("HomeTeamGoals");
            int homeTeamGoals = int.Parse(Console.ReadLine());
            Console.WriteLine("AwayTeamGoals");
            int awayGoals = int.Parse(Console.ReadLine());
            Console.WriteLine("Result  H/A/D");
            string result = Console.ReadLine();
            Console.WriteLine("Referee");
            string referee = Console.ReadLine();

            this.bl.CreateMatch(date, homeTeam, awayTeam, homeTeamGoals, awayGoals, result, referee);
        }

        private void CreatePlayers()
        {
            Console.WriteLine("Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Club");
            string club = Console.ReadLine();
            Console.WriteLine("Age:");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("Position");
            string position = Console.ReadLine();
            Console.WriteLine("Nationality");
            string nationality = Console.ReadLine();
            this.bl.CreatePlayer(name, club, age, position, nationality);
        }

        private void CreateTeam()
        {
            Console.WriteLine("Team name");
            string name = Console.ReadLine();
            Console.WriteLine("Manager");
            string manager = Console.ReadLine();
            Console.WriteLine("Captain");
            string captain = Console.ReadLine();
            Console.WriteLine("KitSponsor");
            string kitsponsor = Console.ReadLine();
            this.bl.CreateTeam(name, manager, captain, kitsponsor);
        }

        private void UpdateMatches()
        {
            Console.WriteLine("ID to update:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Date DD/MM/YYY format:");
            string date = Console.ReadLine();
            Console.WriteLine("HomeTeam:");
            string homeTeam = Console.ReadLine();
            Console.WriteLine("AwayTeam:");
            string awayTeam = Console.ReadLine();
            Console.WriteLine("HomeTeamGoals");
            int homeTeamGoals = int.Parse(Console.ReadLine());
            Console.WriteLine("AwayTeamGoals");
            int awayGoals = int.Parse(Console.ReadLine());
            Console.WriteLine("Result  H/A/D");
            string result = Console.ReadLine();
            Console.WriteLine("Referee");
            string referee = Console.ReadLine();

            this.bl.UpdateMatch(id, date, homeTeam, awayTeam, homeTeamGoals, awayGoals, result, referee);
        }

        private void UpdatePlayers()
        {
            Console.WriteLine("ID:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Name:");
            string name = Console.ReadLine();
            Console.WriteLine("Club");
            string club = Console.ReadLine();
            Console.WriteLine("Age:");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("Position");
            string position = Console.ReadLine();
            Console.WriteLine("Nationality");
            string nationality = Console.ReadLine();
            this.bl.UpdatePlayer(id, name, club, position, nationality, age);
        }

        private void UpdateTeam()
        {
            Console.WriteLine("Team name");
            string name = Console.ReadLine();
            Console.WriteLine("Manager");
            string manager = Console.ReadLine();
            Console.WriteLine("Captain");
            string captain = Console.ReadLine();
            Console.WriteLine("KitSponsor");
            string kitsponsor = Console.ReadLine();
            this.bl.UpdateTeam(name, manager, captain, kitsponsor);
        }

        private void DeleteMatches()
        {
            Console.WriteLine("ID");
            int id = int.Parse(Console.ReadLine());
            this.bl.DeleteMatch(id);
        }

        private void DeletePlayers()
        {
            Console.WriteLine("ID");
            int id = int.Parse(Console.ReadLine());
            this.bl.DeletePlayer(id);
        }

        private void DeleteTeams()
        {
            Console.WriteLine("Team name");
            string name = Console.ReadLine();
            this.bl.DeleteTeam(name);
        }

        private string[] MatchesColumns()
        {
            return typeof(Matches).GetProperties().Where(x => x.PropertyType != typeof(Teams))
         .Select(property => property.Name)
         .ToArray();
        }

        private string[] PlayersColumns()
        {
            return typeof(Players).GetProperties().Where(x => x.PropertyType != typeof(Teams))
         .Select(property => property.Name)
         .ToArray();
        }

        private string[] TeamsColumns()
        {
            return typeof(Teams).GetProperties().Where(x => x.PropertyType != typeof(ICollection<Matches>) && x.PropertyType != typeof(ICollection<Players>))
         .Select(property => property.Name)
         .ToArray();
        }

        private void NumofGamesByRef()
        {
            Console.WriteLine("Name of the referee");
            Console.WriteLine(this.bl.NumberOfMatchesByReferee(Console.ReadLine()));
        }

        private void AverageAgeByTeam()
        {
            Console.WriteLine("Team name");
            Console.WriteLine(this.bl.AverageAgeByTeam(Console.ReadLine()));
        }

        private void ListTeamsBySponsor()
        {
            Console.WriteLine("Sponsor Name");
            foreach (var team in this.bl.TeamsBySponsor(Console.ReadLine()))
            {
                Console.WriteLine(team);
            }
        }

        private void LoadFromXML()
        {
            Console.WriteLine(this.bl.LoadFromXML("http://localhost:8080/PremierLeagueEndPoint/AllTeams"));
        }
    }
}
