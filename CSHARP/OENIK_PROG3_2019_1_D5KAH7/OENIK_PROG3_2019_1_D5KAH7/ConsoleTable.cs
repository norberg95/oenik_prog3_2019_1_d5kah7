﻿// <copyright file="ConsoleTable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace OENIK_PROG3_2019_1_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    /// <summary>
    /// Format of Table
    /// </summary>
    public enum Format
    {
        /// <summary>
        /// Default
        /// </summary>
        Default = 0,

        /// <summary>
        /// Markdown
        /// </summary>
        MarkDown = 1,

        /// <summary>
        /// Alternative
        /// </summary>
        Alternative = 2,

        /// <summary>
        /// Minimal
        /// </summary>
        Minimal = 3
    }

    /// <summary>
    /// Fromatted ConsoleTable
    /// </summary>
    public class ConsoleTable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleTable"/> class.
        /// </summary>
        /// <param name="columns">columns</param>
        public ConsoleTable(params string[] columns)
            : this(new ConsoleTableOptions { Columns = new List<string>(columns) })
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleTable"/> class.
        /// </summary>
        /// <param name="options">name</param>
        public ConsoleTable(ConsoleTableOptions options)
        {
            this.Options = options ?? throw new ArgumentNullException("options");
            this.Rows = new List<object[]>();
            this.Columns = new List<object>(options.Columns);
        }

        /// <summary>
        /// Gets or sets columns
        /// </summary>
        public IList<object> Columns { get; set; }

        /// <summary>
        /// Gets or sets Rows
        /// </summary>
        public IList<object[]> Rows { get; protected set; }

        /// <summary>
        /// Gets or sets Options
        /// </summary>
        public ConsoleTableOptions Options { get; protected set; }

        /// <summary>
        /// From
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="values">Name</param>
        /// <returns>ConsoleTable</returns>
        public static ConsoleTable From<T>(IEnumerable<T> values)
        {
            var table = new ConsoleTable();

            var columns = GetColumns<T>();

            table.AddColumn(columns);

            foreach (var propertyValues in values.Select(value => columns.Select(column => GetColumnValue<T>(value, column))))
            {
                table.AddRow(propertyValues.ToArray());
            }

            return table;
        }

        /// <summary>
        /// Add Column
        /// </summary>
        /// <param name="names">names</param>
        /// <returns>ConsoleTable</returns>
        public ConsoleTable AddColumn(IEnumerable<string> names)
        {
            foreach (var name in names)
            {
                this.Columns.Add(name);
            }

            return this;
        }

        /// <summary>
        /// Add row
        /// </summary>
        /// <param name="values">values</param>
        /// <returns>ConsoleTable</returns>
        public ConsoleTable AddRow(params object[] values)
        {
            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            if (!this.Columns.Any())
            {
                throw new Exception("Please set the columns first");
            }

            if (this.Columns.Count != values.Length)
            {
                throw new Exception(
                    $"The number columns in the row ({this.Columns.Count}) does not match the values ({values.Length}");
            }

            this.Rows.Add(values);
            return this;
        }

        /// <summary>
        /// Override ToString
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            var builder = new StringBuilder();

            // find the longest column by searching each row
            var columnLengths = this.ColumnLengths();

            // create the string format with padding
            var format = Enumerable.Range(0, this.Columns.Count)
                .Select(i => " | {" + i + ",-" + columnLengths[i] + "}")
                .Aggregate((s, a) => s + a) + " |";

            // find the longest formatted line
            var maxRowLength = Math.Max(0, this.Rows.Any() ? this.Rows.Max(row => string.Format(format, row).Length) : 0);
            var columnHeaders = string.Format(format, this.Columns.ToArray());

            // longest line is greater of formatted columnHeader and longest row
            var longestLine = Math.Max(maxRowLength, columnHeaders.Length);

            // add each row
            var results = this.Rows.Select(row => string.Format(format, row)).ToList();

            // create the divider
            var divider = " " + string.Join(string.Empty, Enumerable.Repeat("-", longestLine - 1)) + " ";

            builder.AppendLine(divider);
            builder.AppendLine(columnHeaders);

            foreach (var row in results)
            {
                builder.AppendLine(divider);
                builder.AppendLine(row);
            }

            builder.AppendLine(divider);

            if (this.Options.EnableCount)
            {
                builder.AppendLine(string.Empty);
                builder.AppendFormat(" Count: {0}", this.Rows.Count);
            }

            return builder.ToString();
        }

        /// <summary>
        /// To mark down string
        /// </summary>
        /// <returns>string</returns>
        public string ToMarkDownString()
        {
            return this.ToMarkDownString('|');
        }

        /// <summary>
        /// To minimal string
        /// </summary>
        /// <returns>string</returns>
        public string ToMinimalString()
        {
            return this.ToMarkDownString(char.MinValue);
        }

        /// <summary>
        /// To string alterative
        /// </summary>
        /// <returns>string</returns>
        public string ToStringAlternative()
        {
            var builder = new StringBuilder();

            // find the longest column by searching each row
            var columnLengths = this.ColumnLengths();

            // create the string format with padding
            var format = this.Format(columnLengths);

            // find the longest formatted line
            var columnHeaders = string.Format(format, this.Columns.ToArray());

            // add each row
            var results = this.Rows.Select(row => string.Format(format, row)).ToList();

            // create the divider
            var divider = Regex.Replace(columnHeaders, @"[^|]", "-");
            var dividerPlus = divider.Replace("|", "+");

            builder.AppendLine(dividerPlus);
            builder.AppendLine(columnHeaders);

            foreach (var row in results)
            {
                builder.AppendLine(dividerPlus);
                builder.AppendLine(row);
            }

            builder.AppendLine(dividerPlus);

            return builder.ToString();
        }

        /// <summary>
        /// Write
        /// </summary>
        /// <param name="format">format</param>
        public void Write(Format format = OENIK_PROG3_2019_1_D5KAH7.Format.Default)
        {
            switch (format)
            {
                case OENIK_PROG3_2019_1_D5KAH7.Format.Default:
                    Console.WriteLine(this.ToString());
                    break;
                case OENIK_PROG3_2019_1_D5KAH7.Format.MarkDown:
                    Console.WriteLine(this.ToMarkDownString());
                    break;
                case OENIK_PROG3_2019_1_D5KAH7.Format.Alternative:
                    Console.WriteLine(this.ToStringAlternative());
                    break;
                case OENIK_PROG3_2019_1_D5KAH7.Format.Minimal:
                    Console.WriteLine(this.ToMinimalString());
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(format), format, null);
            }
        }

        private static IEnumerable<string> GetColumns<T>()
        {
            return typeof(T).GetProperties().Select(x => x.Name).ToArray();
        }

        private static object GetColumnValue<T>(object target, string column)
        {
            return typeof(T).GetProperty(column).GetValue(target, null);
        }

        private string ToMarkDownString(char delimiter)
        {
            var builder = new StringBuilder();

            // find the longest column by searching each row
            var columnLengths = this.ColumnLengths();

            // create the string format with padding
            var format = this.Format(columnLengths, delimiter);

            // find the longest formatted line
            var columnHeaders = string.Format(format, this.Columns.ToArray());

            // add each row
            var results = this.Rows.Select(row => string.Format(format, row)).ToList();

            // create the divider
            var divider = Regex.Replace(columnHeaders, @"[^|]", "-");

            builder.AppendLine(columnHeaders);
            builder.AppendLine(divider);
            results.ForEach(row => builder.AppendLine(row));

            return builder.ToString();
        }

        private string Format(List<int> columnLengths, char delimiter = '|')
        {
            var delimiterStr = delimiter == char.MinValue ? string.Empty : delimiter.ToString();
            var format = (Enumerable.Range(0, this.Columns.Count)
                .Select(i => " " + delimiterStr + " {" + i + ",-" + columnLengths[i] + "}")
                .Aggregate((s, a) => s + a) + " " + delimiterStr).Trim();
            return format;
        }

        private List<int> ColumnLengths()
        {
            var columnLengths = this.Columns
                .Select((t, i) => this.Rows.Select(x => x[i])
                    .Union(new[] { this.Columns[i] })
                    .Where(x => x != null)
                    .Select(x => x.ToString().Length).Max())
                .ToList();
            return columnLengths;
        }
    }
}
