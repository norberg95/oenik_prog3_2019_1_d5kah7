﻿// <copyright file="ConsoleTableOptions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace OENIK_PROG3_2019_1_D5KAH7
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Console table options
    /// </summary>
    public class ConsoleTableOptions
    {
        /// <summary>
        /// Gets or sets Columns
        /// </summary>
        public IEnumerable<string> Columns { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets a value whether the count is true or false
        /// </summary>
        public bool EnableCount { get; set; } = true;
    }
}
