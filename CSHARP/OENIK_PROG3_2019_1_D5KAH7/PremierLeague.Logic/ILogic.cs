﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PremierLeague.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PremierLeague.Data;

    /// <summary>
    /// Interface for Logic
    /// </summary>
    internal interface ILogic
    {
        /// <summary>
        /// Creates a player
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="club">club</param>
        /// <param name="age">age</param>
        /// <param name="position">position</param>
        /// <param name="nationality">nationality</param>
        /// <returns>bool</returns>
        bool CreatePlayer(string name, string club, int age, string position, string nationality);

        /// <summary>
        /// Reads Player table
        /// </summary>
        /// <returns>List</returns>
        IQueryable<Players> ReadPlayerTable();

        /// <summary>
        /// Updates player
        /// </summary>
        /// <param name="oldID">id to update</param>
        /// <param name="name">name</param>
        /// <param name="club">club</param>
        /// <param name="position">position</param>
        /// <param name="nationality">nationality</param>
        /// <param name="age">age</param>
        /// <returns>bool</returns>
        bool UpdatePlayer(int oldID,  string name, string club, string position, string nationality, int age);

        /// <summary>
        /// Delete player
        /// </summary>
        /// <param name="iD">id to delete</param>
        /// <returns>bool</returns>
        bool DeletePlayer(int iD);

        /// <summary>
        /// Creates match
        /// </summary>
        /// <param name="date">date</param>
        /// <param name="homeTeam">Home team</param>
        /// <param name="awayTeam">away team</param>
        /// <param name="homeTeamGoals">home team goals</param>
        /// <param name="awayTeamGoals">away team goals</param>
        /// <param name="result">result</param>
        /// <param name="referee">referee</param>
        /// <returns>bool</returns>
        bool CreateMatch(string date, string homeTeam, string awayTeam, int homeTeamGoals, int awayTeamGoals, string result, string referee);

        /// <summary>
        /// Reads matches table
        /// </summary>
        /// <returns>list</returns>
        IQueryable<Matches> ReadMatchesTable();

        /// <summary>
        /// Update a match entity
        /// </summary>
        /// <param name="oldID">id to update</param>
        /// <param name="date">date</param>
        /// <param name="homeTeam">home team</param>
        /// <param name="awayTeam">away team</param>
        /// <param name="homeTeamGoals">home team goals</param>
        /// <param name="awayTeamGoals">away team goals</param>
        /// <param name="result">result</param>
        /// <param name="referee">referee</param>
        /// <returns>bool</returns>
        bool UpdateMatch(int oldID, string date, string homeTeam, string awayTeam, int homeTeamGoals, int awayTeamGoals, string result, string referee);

        /// <summary>
        /// Delete match
        /// </summary>
        /// <param name="iD">id</param>
        /// <returns>bool</returns>
        bool DeleteMatch(int iD);

        /// <summary>
        /// Creates team
        /// </summary>
        /// <param name="team">team name</param>
        /// <param name="manager">manager</param>
        /// <param name="captain">captain</param>
        /// <param name="kitSponsor">kitsponsor</param>
        /// <returns>bool</returns>
        bool CreateTeam(string team, string manager, string captain, string kitSponsor);

        /// <summary>
        /// Read teams
        /// </summary>
        /// <returns>list</returns>
        IQueryable<Teams> ReadTeamsTable();

        /// <summary>
        /// Update team
        /// </summary>
        /// <param name="oldTeam">old team</param>
        /// <param name="manager">manager</param>
        /// <param name="captain">captain</param>
        /// <param name="kitSponsor">kit sponsor</param>
        /// <returns>bool</returns>
        bool UpdateTeam(string oldTeam,  string manager, string captain, string kitSponsor);

        /// <summary>
        /// Deletes team
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>bool</returns>
        bool DeleteTeam(string name);

        /// <summary>
        /// Number of matches by referee
        /// </summary>
        /// <param name="name">ref name</param>
        /// <returns>int</returns>
        int NumberOfMatchesByReferee(string name);

        /// <summary>
        /// Average age by a team
        /// </summary>
        /// <param name="team">team name</param>
        /// <returns>average</returns>
        double AverageAgeByTeam(string team);

        /// <summary>
        /// Teams by sponor
        /// </summary>
        /// <param name="sponsor">sponsor</param>
        /// <returns>list of teams</returns>
        List<string> TeamsBySponsor(string sponsor);

        /// <summary>
        /// Load an xml
        /// </summary>
        /// <param name="url">url</param>
        /// <returns>xml string</returns>
        string LoadFromXML(string url);
    }
}
