﻿// <copyright file="BL.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PremierLeague.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using PremierLeague.Data;
    using PremierLeague.Repository;

    /// <summary>
    /// Logic
    /// </summary>
    public class BL : ILogic
    {
        private RepositoryContainer repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BL"/> class.
        /// </summary>
        public BL()
        {
            this.Repo = new RepositoryContainer();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BL"/> class.
        /// </summary>
        /// <param name="repo"> repo</param>
        public BL(RepositoryContainer repo)
        {
            this.Repo = repo;
        }

        /// <summary>
        /// Gets or sets the repo
        /// </summary>
        public RepositoryContainer Repo { get => this.repo; set => this.repo = value; }

        /// <summary>
        /// Average age by team
        /// </summary>
        /// <param name="team">team</param>
        /// <returns>average</returns>
        public double AverageAgeByTeam(string team)
        {
            return this.Repo.PlayersRepo.Read().Where(x => x.Club == team).Average(x => x.Age);
        }

        /// <summary>
        /// Creates a natch
        /// </summary>
        /// <param name="date">date</param>
        /// <param name="homeTeam">home team</param>
        /// <param name="awayTeam">away team</param>
        /// <param name="homeTeamGoals">home team goals</param>
        /// <param name="awayTeamGoals">away team goals</param>
        /// <param name="result">resut</param>
        /// <param name="referee">referee</param>
        /// <returns>bool</returns>
        public bool CreateMatch(string date, string homeTeam, string awayTeam, int homeTeamGoals, int awayTeamGoals, string result, string referee)
        {
            try
            {
                return this.Repo.MatchesRepo.Create(new Matches()
                {
                    // MatchID = ID,
                    Date = date,
                    HomeTeam = homeTeam,
                    AwayTeam = awayTeam,
                    HomeTeamGoals = homeTeamGoals,
                    AwayTeamGoals = awayTeamGoals,
                    Result = result,
                    Referee = referee
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("CreateMatch error " + e.Message);
            }

            return false;
        }

        /// <summary>
        /// Create player
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="club">club</param>
        /// <param name="age">age</param>
        /// <param name="position">position</param>
        /// <param name="nationality">nationality</param>
        /// <returns>bool</returns>
        public bool CreatePlayer(string name, string club, int age, string position, string nationality)
        {
            try
            {
                return this.Repo.PlayersRepo.Create(new Players()
                {
                    // ClubID = ID,
                    Name = name,
                    Club = club,
                    Age = age,
                    Position = position,
                    Nationality = nationality
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("CreatePlayer error at" + e.Message);
            }

            return false;
        }

        /// <summary>
        /// Creates team
        /// </summary>
        /// <param name="team">team</param>
        /// <param name="manager">manager</param>
        /// <param name="captain">captain</param>
        /// <param name="kitSponsor">kit sponsor</param>
        /// <returns>bool</returns>
        public bool CreateTeam(string team, string manager, string captain, string kitSponsor)
        {
            try
            {
                return this.Repo.TeamsRepo.Create(new Teams()
                {
                  Team = team,
                  Manager = manager,
                  Captain = captain,
                  KitSponsor = kitSponsor
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("CreateTeam error at" + team + " " + e.Message);
            }

            return false;
        }

        /// <summary>
        /// Delete match
        /// </summary>
        /// <param name="iD">id</param>
        /// <returns>bool</returns>
        public bool DeleteMatch(int iD)
        {
            try
            {
                return this.Repo.MatchesRepo.Delete(iD);
            }
            catch (Exception e)
            {
                Console.WriteLine("DeleteMatch error at" + iD + " " + e.Message);
            }

            return false;
        }

        /// <summary>
        /// Delete Player
        /// </summary>
        /// <param name="iD">id</param>
        /// <returns>bool</returns>
        public bool DeletePlayer(int iD)
        {
            try
            {
                return this.Repo.PlayersRepo.Delete(iD);
            }
            catch (Exception e)
            {
                Console.WriteLine("DeletePlayers error at" + iD + " " + e.Message);
            }

            return false;
        }

        /// <summary>
        /// Delete team
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>bool</returns>
        public bool DeleteTeam(string name)
        {
            try
            {
                return this.Repo.TeamsRepo.Delete(name);
            }
            catch (Exception e)
            {
                Console.WriteLine("DeleteTeam error at" + name + " " + e.Message);
            }

            return false;
        }

        /// <summary>
        /// Load an xml file
        /// </summary>
        /// <param name="url">url</param>
        /// <returns>string</returns>
        public string LoadFromXML(string url)
        {
            return XDocument.Load(url).ToString();
        }

        /// <summary>
        /// Number of matches by a referee
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>int</returns>
        public int NumberOfMatchesByReferee(string name)
        {
            return this.Repo.MatchesRepo.Read().Where(x => x.Referee == name).Count();
        }

        /// <summary>
        /// Read matches table
        /// </summary>
        /// <returns>list</returns>
        public IQueryable<Matches> ReadMatchesTable()
        {
            return this.Repo.MatchesRepo.Read();
        }

        /// <summary>
        /// Read player table
        /// </summary>
        /// <returns>list</returns>
        public IQueryable<Players> ReadPlayerTable()
        {
            return this.Repo.PlayersRepo.Read();
        }

        /// <summary>
        /// Read team table
        /// </summary>
        /// <returns>list</returns>
        public IQueryable<Teams> ReadTeamsTable()
        {
            return this.Repo.TeamsRepo.Read();
        }

        /// <summary>
        /// Teams by sponsor
        /// </summary>
        /// <param name="sponsor">sponsor</param>
        /// <returns>list of teams sponsored by a brand</returns>
        public List<string> TeamsBySponsor(string sponsor)
        {
            return this.Repo.TeamsRepo.Read().Where(x => x.KitSponsor == sponsor).Select(x => x.Team).ToList();
        }

        /// <summary>
        /// Update match
        /// </summary>
        /// <param name="oldID">id to update</param>
        /// <param name="date">date</param>
        /// <param name="homeTeam">home team</param>
        /// <param name="awayTeam">away team</param>
        /// <param name="homeTeamGoals">home team goals</param>
        /// <param name="awayTeamGoals">away team goals</param>
        /// <param name="result">result</param>
        /// <param name="referee">referee</param>
        /// <returns>bool</returns>
        public bool UpdateMatch(int oldID,  string date, string homeTeam, string awayTeam, int homeTeamGoals, int awayTeamGoals, string result, string referee)
        {
            try
            {
                Matches updated = new Matches()
                {
                    Date = date,
                    HomeTeam = homeTeam,
                    AwayTeam = awayTeam,
                    HomeTeamGoals = homeTeamGoals,
                    AwayTeamGoals = awayTeamGoals,
                    Result = result,
                    Referee = referee
                };
                return this.Repo.MatchesRepo.Update(
                    oldID,
                    updated);
            }
            catch (Exception e)
            {
                Console.WriteLine("UpdateMatch error at" + oldID + " " + e.Message);
            }

            return false;
        }

        /// <summary>
        /// Update players
        /// </summary>
        /// <param name="oldID">id to update</param>
        /// <param name="name">name</param>
        /// <param name="club">club</param>
        /// <param name="position">position</param>
        /// <param name="nationality"> nationality</param>
        /// <param name="age">age</param>
        /// <returns>bool</returns>
        public bool UpdatePlayer(int oldID,  string name, string club, string position, string nationality, int age)
        {
            try
            {
                Players updated = new Players()
                {
                    Name = name,
                    Club = club,
                    Position = position,
                    Nationality = nationality,
                    Age = age
                };
                return this.Repo.PlayersRepo.Update(
                    oldID,
                    updated);
            }
            catch (Exception e)
            {
                Console.WriteLine("UpdatePlayer error at" + oldID + " " + e.Message);
            }

            return false;
        }

        /// <summary>
        /// Update team
        /// </summary>
        /// <param name="oldTeam"> old team</param>
        /// <param name="manager"> manager</param>
        /// <param name="captain">captain </param>
        /// <param name="kitSponsor">sponsor</param>
        /// <returns>bool</returns>
        public bool UpdateTeam(string oldTeam,  string manager, string captain, string kitSponsor)
        {
            try
            {
                Teams updated = new Teams()
                {
                    Manager = manager,
                    Captain = captain,
                    KitSponsor = kitSponsor
                };
                return this.Repo.TeamsRepo.Update(
                    oldTeam,
                    updated);
            }
            catch (Exception e)
            {
                Console.WriteLine("UpdateTeam error at" + oldTeam + " " + e.Message);
            }

            return false;
        }
    }
}
