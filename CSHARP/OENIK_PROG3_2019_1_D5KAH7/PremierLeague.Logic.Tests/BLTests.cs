﻿// <copyright file="BLTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PremierLeague.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PremierLeague.Data;
    using PremierLeague.Logic;
    using PremierLeague.Repository;

    /// <summary>
    /// Tests for logic
    /// </summary>
    [TestFixture]
    public class BLTests
    {
        private Mock<IRepository<Matches>> matchesMock;
        private Mock<IRepository<Players>> playersMock;
        private Mock<IRepository<Teams>> teamsMock;
        private BL logic;

        /// <summary>
        /// Setup for testing
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.matchesMock = new Mock<IRepository<Matches>>();
            this.playersMock = new Mock<IRepository<Players>>();
            this.teamsMock = new Mock<IRepository<Teams>>();

            this.logic = new BL(new RepositoryContainer(this.matchesMock.Object, this.playersMock.Object, this.teamsMock.Object));

            List<Matches> matchList = new List<Matches>()
            {
                new Matches()
            {
                Date = "30/12/2017",
                HomeTeam = "Arsenal",
                AwayTeam = "Tottenham",
                HomeTeamGoals = 2,
                AwayTeamGoals = 5,
                Result = "A",
                Referee = "M Dean"
            },
                new Matches()
                {
                Date = "30/12/2019",
                HomeTeam = "Everton",
                AwayTeam = "West Ham",
                HomeTeamGoals = 4,
                AwayTeamGoals = 4,
                Result = "D",
                Referee = "M Atkinson"
                }
            };

            this.matchesMock.Setup(x => x.Read()).Returns(matchList.AsQueryable());
            this.matchesMock.Setup(x => x.Create(matchList.ElementAt(0))).Returns(true);
            this.matchesMock.Setup(x => x.Delete(300)).Returns(true);
            Matches updated1 = new Matches()
            {
                Date = "30/12/2019",
                HomeTeam = "Chelsea",
                AwayTeam = "West Ham",
                HomeTeamGoals = 7,
                AwayTeamGoals = 7,
                Result = "D",
                Referee = "M Atkinson"
            };
            this.matchesMock.Setup(x => x.Update(381, updated1));

            List<Players> playerList = new List<Players>()
            {
                new Players()
                {
                    Name = "Rajos Norbert",
                    Club = "Tottenham",
                    Age = 23,
                    Position = "CB",
                    Nationality = "Hungarian"
                },

                new Players()
                {
                    Name = "Gareth Bale",
                    Club = "Tottenham",
                    Age = 28,
                    Position = "LW",
                    Nationality = "Welsh"
                }
            };

            this.playersMock.Setup(x => x.Read()).Returns(playerList.AsQueryable());
            this.playersMock.Setup(x => x.Create(playerList.ElementAt(0))).Returns(true);
            this.playersMock.Setup(x => x.Delete(300)).Returns(true);
            Players updated = new Players()
            {
                Name = "Gareth Bale",
                Club = "Tottenham",
                Age = 28,
                Position = "LW",
                Nationality = "Welsh"
            };
            this.playersMock.Setup(x => x.Update(381, updated));

            List<Teams> teamList = new List<Teams>()
            {
                new Teams()
                {
                   Team = "Leeds",
                   Manager = "Bielsa",
                   Captain = "Jack Clarke",
                   KitSponsor = "Puma"
                },

                new Teams()
                {
                    Team = "Ipswich Town",
                   Manager = "Zinedine Zidane",
                   Captain = "Jack Grealish",
                   KitSponsor = "Puma"
                }
            };

            this.teamsMock.Setup(x => x.Read()).Returns(teamList.AsQueryable());
            this.teamsMock.Setup(x => x.Create(teamList.ElementAt(0))).Returns(true);
            this.teamsMock.Setup(x => x.Delete("Arsenal")).Returns(true);
            Teams updated2 = new Teams()
            {
                Team = "Arsenal",
                Manager = "Zinedine Zidane",
                Captain = "Jack Grealish",
                KitSponsor = "Nike"
            };
            this.teamsMock.Setup(x => x.Update(381, updated2));
        }

        /// <summary>
        /// Test for MatchesCreate
        /// </summary>
        [Test]
        public void MatchesCreateTest()
        {
            this.logic.CreateMatch("30/17/2017", "Arsenal", "Everton", 3, 3, "D", "M Dean");
            this.matchesMock.Verify(x => x.Create(It.IsAny<Matches>()), Times.Once);
        }

        /// <summary>
        /// Test for PlayersCreate
        /// </summary>
        [Test]
        public void PlayersCreateTest()
        {
            this.logic.CreatePlayer("Fernando Torres", "Chelsea", 32, "FW", "Spanish");
            this.playersMock.Verify(x => x.Create(It.IsAny<Players>()), Times.Once);
        }

        /// <summary>
        /// Test for TeamsCreate
        /// </summary>
        [Test]
        public void TeamsCreateTest()
        {
            this.logic.CreateTeam("Fradi", "Pinyő", "Dombi", "2Rule");
            this.teamsMock.Verify(x => x.Create(It.IsAny<Teams>()), Times.Once);
        }

        /// <summary>
        /// Test for MatchesRead
        /// </summary>
        [Test]
        public void MatchesReadTest()
        {
            int count = this.logic.ReadMatchesTable().Count();
            Assert.AreEqual(count, 2);
        }

        /// <summary>
        /// Test for PlayersRead
        /// </summary>
        [Test]
        public void PlayersReadTest()
        {
            int count = this.logic.ReadPlayerTable().Count();
            Assert.AreEqual(count, 2);
        }

        /// <summary>
        /// Test for TeamRead
        /// </summary>
        [Test]
        public void TeamsReadTest()
        {
            int count = this.logic.ReadTeamsTable().Count();
            Assert.AreEqual(count, 2);
        }

        /// <summary>
        /// Test for MatchesUpdate
        /// </summary>
        [Test]
        public void MatchesUpdateTest()
        {
            this.logic.UpdateMatch(382, "11/11/2011", "Liverpool", "Tottenham", 0, 7, "A", "H Webb");
            this.matchesMock.Verify(x => x.Update(It.IsAny<object>(), It.IsAny<Matches>()), Times.Once);
        }

        /// <summary>
        /// Test for PlayersUpdate
        /// </summary>
        [Test]
        public void PlayersUpdateTest()
        {
            this.logic.UpdatePlayer(381, "Alvaro Morata", "Chelsea", "FW", "Spanish", 27);
            this.playersMock.Verify(x => x.Update(It.IsAny<object>(), It.IsAny<Players>()), Times.Once);
        }

        /// <summary>
        /// Test for TeamsUpdate
        /// </summary>
        [Test]
        public void TeamsUpdateTest()
        {
            this.logic.UpdateTeam("Arsenal", "Arsene Wenger", "Henry", "Puma");
            this.teamsMock.Verify(x => x.Update(It.IsAny<object>(), It.IsAny<Teams>()), Times.Once);
        }

        /// <summary>
        /// Test for MatchesDelete
        /// </summary>
        [Test]
        public void MatchesDeleteTest()
        {
            this.logic.DeleteMatch(300);
            this.matchesMock.Verify(x => x.Delete(It.IsAny<object>()), Times.Once);
        }

        /// <summary>
        /// Test for playersDelete
        /// </summary>
        [Test]
        public void PlayersDeleteTest()
        {
            this.logic.DeletePlayer(300);
            this.playersMock.Verify(x => x.Delete(It.IsAny<object>()), Times.Once);
        }

        /// <summary>
        /// Test for TeamsDelete
        /// </summary>
        [Test]
        public void TeamsDeleteTest()
        {
            this.logic.DeleteTeam("Arsenal");
            this.teamsMock.Verify(x => x.Delete(It.IsAny<object>()), Times.Once);
        }

        /// <summary>
        /// Test for AverageAgeByTeam
        /// </summary>
        [Test]
        public void AverageAgeByTeamTest()
        {
            Assert.AreEqual(this.logic.AverageAgeByTeam("Tottenham"), 25.5);
        }

        /// <summary>
        /// Test for NumberOfMatchesByReferee
        /// </summary>
        [Test]
        public void NumberOfMatchesByRefereeTest()
        {
            Assert.AreEqual(this.logic.NumberOfMatchesByReferee("M Atkinson"), 1);
        }

        /// <summary>
        /// Test fot TeamsBySponsor
        /// </summary>
        [Test]
        public void TeamsBySponsorTest()
        {
            Assert.AreEqual(this.logic.TeamsBySponsor("Puma"), new string[] { "Leeds", "Ipswich Town" });
        }
    }
}
