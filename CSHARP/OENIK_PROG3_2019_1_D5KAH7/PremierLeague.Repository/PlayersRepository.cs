﻿// <copyright file="PlayersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PremierLeague.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PremierLeague.Data;

    /// <summary>
    /// Players repo
    /// </summary>
    public class PlayersRepository : IRepository<Players>
    {
        private DbSet<Players> playersTable;
        private PremierLeagueDataModel parent;

        /// <summary>
        /// Gets or sets Dataset of players
        /// </summary>
        public DbSet<Players> PlayersTable { get => this.playersTable; set => this.playersTable = value; }

        /// <summary>
        /// Gets or sets the data model
        /// </summary>
        public PremierLeagueDataModel Parent { get => this.parent; set => this.parent = value; }

        /// <summary>
        /// Creates a player
        /// </summary>
        /// <param name="entity">entity</param>
        /// <returns>bool</returns>
        public bool Create(Players entity)
        {
            this.PlayersTable.Add(entity);
            this.Parent.SaveChanges();
            return true;
        }

        /// <summary>
        /// Deletes player
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>bool</returns>
        public bool Delete(object id)
        {
            var temp = this.PlayersTable.First(x => x.ClubID.Equals((int)id));
            if (temp != null)
            {
                this.PlayersTable.Remove(temp);
                this.Parent.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Reads the players
        /// </summary>
        /// <returns>list of players</returns>
        public IQueryable<Players> Read()
        {
            return this.PlayersTable.Select(x => x);
        }

        /// <summary>
        /// Updates a player
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="updated">updated attrbutes</param>
        /// <returns>bool</returns>
        public bool Update(object id, Players updated)
        {
            Players old = this.Read().Where(x => x.ClubID == (int)id).Select(x => x).FirstOrDefault();
            if (old != null)
            {
                old.Age = updated.Age;
                old.Club = updated.Club;
                old.Name = updated.Name;
                old.Nationality = updated.Nationality;
                old.Position = updated.Position;

                this.Parent.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
