﻿// <copyright file="TeamsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PremierLeague.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PremierLeague.Data;

    /// <summary>
    /// Repository for the teams.
    /// </summary>
    public class TeamsRepository : IRepository<Teams>
    {
        private DbSet<Teams> teamsTable;

        private PremierLeagueDataModel parent;

        /// <summary>
        /// Gets or sets the Teams Dataset.
        /// </summary>
        public DbSet<Teams> TeamsTable { get => this.teamsTable; set => this.teamsTable = value; }

        /// <summary>
        /// Gets or sets the DataModel
        /// </summary>
        public PremierLeagueDataModel Parent { get => this.parent; set => this.parent = value; }

        /// <summary>
        /// Creates a team.
        /// </summary>
        /// <param name="entity">entity</param>
        /// <returns>If it was successful.</returns>
        public bool Create(Teams entity)
        {
            this.TeamsTable.Add(entity);
            this.Parent.SaveChanges();
            return true;
        }

        /// <summary>
        /// Deletes a team.
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>If it was succesfull.</returns>
        public bool Delete(object id)
        {
            var temp = this.TeamsTable.First(x => x.Team == id.ToString());
            if (temp != null)
            {
                this.TeamsTable.Remove(temp);
                this.Parent.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets all entities of Teams dataset.
        /// </summary>
        /// <returns>The list of teams.</returns>
        public IQueryable<Teams> Read()
        {
            return this.TeamsTable.Select(x => x);
        }

        /// <summary>
        /// Update a record in Teams table.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="updated">updated</param>
        /// <returns>If it was succesfull.</returns>
        public bool Update(object id, Teams updated)
        {
            Teams old = this.Read().Where(x => x.Team == id.ToString()).Select(x => x).FirstOrDefault();

            if (old != null)
            {
                old.Captain = updated.Captain;
                old.KitSponsor = updated.KitSponsor;
                old.Manager = updated.Manager;

                this.Parent.SaveChanges();
                return true;
            }

            return true;
        }
    }
}
