﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PremierLeague.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the repos.
    /// </summary>
    /// <typeparam name="T">Any parameter type</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Read method for repo.
        /// </summary>
        /// <returns>The list of elements.</returns>
        IQueryable<T> Read();

        /// <summary>
        /// Create an entity.
        /// </summary>
        /// <param name="entity">the entitiy</param>
        /// <returns>if it was successful</returns>
        bool Create(T entity);

        /// <summary>
        /// Updates an entity
        /// </summary>
        /// <param name="id">Id to update</param>
        /// <param name="updated">details of update</param>
        /// <returns>if it was successful</returns>
        bool Update(object id, T updated);

        /// <summary>
        /// Deletes the entity
        /// </summary>
        /// <param name="id">the id to delete</param>
        /// <returns>if it was succescsful</returns>
        bool Delete(object id);
    }
}
