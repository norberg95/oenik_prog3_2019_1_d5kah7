﻿// <copyright file="MatchesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PremierLeague.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PremierLeague.Data;

    /// <summary>
    /// Repo for matches
    /// </summary>
    public class MatchesRepository : IRepository<Matches>
    {
        private DbSet<Matches> matchesTable;
        private PremierLeagueDataModel parent;

        /// <summary>
        /// Gets or sets the matches dataset
        /// </summary>
        public DbSet<Matches> MatchesTable { get => this.matchesTable; set => this.matchesTable = value; }

        /// <summary>
        /// Gets or sets the datamodel.
        /// </summary>
        public PremierLeagueDataModel Parent { get => this.parent; set => this.parent = value; }

        /// <summary>
        /// Creates an entity
        /// </summary>
        /// <param name="entity">entity</param>
        /// <returns>bool</returns>
        public bool Create(Matches entity)
        {
            this.MatchesTable.Add(entity);
            this.Parent.SaveChanges();
            return true;
        }

        /// <summary>
        /// Deletes entity
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>bool</returns>
        public bool Delete(object id)
        {
            var temp = this.Read().First(x => x.MatchID.Equals((int)id));
            if (temp != null)
            {
                this.MatchesTable.Remove(temp);
                this.Parent.SaveChanges();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Read the entities
        /// </summary>
        /// <returns>list of matches</returns>
        public IQueryable<Matches> Read()
        {
            return this.MatchesTable.Select(x => x);
        }

        /// <summary>
        /// Updates an entity
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="updated">updated attributes</param>
        /// <returns>bool</returns>
        public bool Update(object id, Matches updated)
        {
            Matches old = this.Read().Where(x => x.MatchID == (int)id).Select(x => x).FirstOrDefault();
            if (old != null)
            {
                old.Date = updated.Date;
                old.AwayTeam = updated.AwayTeam;
                old.AwayTeamGoals = updated.AwayTeamGoals;
                old.HomeTeam = updated.HomeTeam;
                old.HomeTeamGoals = updated.HomeTeamGoals;
                old.Referee = updated.Referee;
                old.Result = updated.Result;
                this.Parent.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
