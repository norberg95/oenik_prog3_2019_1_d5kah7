﻿// <copyright file="RepositoryContainer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PremierLeague.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PremierLeague.Data;

    /// <summary>
    /// Container for the repositories
    /// </summary>
    public class RepositoryContainer
    {
        private PremierLeagueDataModel premierLeagueDataModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryContainer"/> class.
        /// </summary>
        public RepositoryContainer()
        {
            this.PremierLeagueDataModel = new PremierLeagueDataModel();
            this.PlayersRepo = new PlayersRepository();
            this.TeamsRepo = new TeamsRepository();
            this.MatchesRepo = new MatchesRepository();
            (this.TeamsRepo as TeamsRepository).TeamsTable = this.PremierLeagueDataModel.Teams;
            (this.TeamsRepo as TeamsRepository).Parent = this.PremierLeagueDataModel;
            (this.PlayersRepo as PlayersRepository).PlayersTable = this.PremierLeagueDataModel.Players;
            (this.PlayersRepo as PlayersRepository).Parent = this.PremierLeagueDataModel;
            (this.MatchesRepo as MatchesRepository).MatchesTable = this.PremierLeagueDataModel.Matches;
            (this.MatchesRepo as MatchesRepository).Parent = this.PremierLeagueDataModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryContainer"/> class.
        /// </summary>
        /// <param name="matchesRepo">Matches repo</param>
        /// <param name="playersRepo">Players repo</param>
        /// <param name="teamsRepo">Teams repo</param>
        public RepositoryContainer(IRepository<Matches> matchesRepo, IRepository<Players> playersRepo, IRepository<Teams> teamsRepo)
        {
            this.PremierLeagueDataModel = new PremierLeagueDataModel();
            this.PlayersRepo = playersRepo;
            this.TeamsRepo = teamsRepo;
            this.MatchesRepo = matchesRepo;
        }

        /// <summary>
        /// Gets or sets the Players repo
        /// </summary>
        public IRepository<Players> PlayersRepo { get; set; }

        /// <summary>
        /// Gets or sets the Teams repo
        /// </summary>
        public IRepository<Teams> TeamsRepo { get; set; }

        /// <summary>
        /// Gets or sets the Matches repo
        /// </summary>
        public IRepository<Matches> MatchesRepo { get; set; }

        /// <summary>
        /// Gets or sets the data model
        /// </summary>
        public PremierLeagueDataModel PremierLeagueDataModel { get => this.premierLeagueDataModel; set => this.premierLeagueDataModel = value; }
    }
}
