/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import model.Teams;
import model.Players;
import model.QueriedTeams;

/**
 *
 * @author Admin
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PremierLeagueService implements Serializable {
   
    
    private static PremierLeagueService instance;

    public PremierLeagueService() {
        Teams tottenham=new Teams();//"Tottenham","Mauricio Pochettino","Hugo LLoris","Nike"
        tottenham.setTeam("Tottenham");
        tottenham.setManager("Mauricio Pochettino");
        tottenham.setCaptain("Hugo LLoris");
        tottenham.setKitSponsor("Nike");
        
        Players hugo=new Players();//"Hugo LLoris","Tottenham",33,"GK","French"
        hugo.setName("Hugo LLoris");
        hugo.setClub("Tottenham");
        hugo.setAge(33);
        hugo.setPosition("GK");
        hugo.setNationality("French");
        tottenham.getPlayers().add(hugo);
        
        
        teams.add(tottenham);
        //Teams chelsea=new Teams("Chelsea","Maurizio Sarri","Jhon Terry","Puma");
        //Players terry=new Players("John Terry","Chelsea",37,"CB","English");
        //chelsea.getPlayers().add(terry);
        //teams.add(chelsea);
    }
    public static PremierLeagueService getInstance()
    {
        if (instance==null) {
            instance= new PremierLeagueService();
        }
        return instance;
    }
     @XmlElement
     private List<Teams> teams= new ArrayList<>();

    public List<Teams> getTeams() {
        return teams;
    }
    
    public QueriedTeams getQueriedTeams(String team){
        QueriedTeams local = new QueriedTeams();
        for (Teams b : teams) {
            if (b.getTeam().toLowerCase().trim().equals(team.toLowerCase().trim())) {
                //local.setQueriedList((ArrayList<CarModel>) b.getModels());
                local.setTeam(b);
            }
        }
                
        return local;
    }
}
