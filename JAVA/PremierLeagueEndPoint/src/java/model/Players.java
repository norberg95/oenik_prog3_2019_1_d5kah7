/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@XmlRootElement(name ="player")
@XmlAccessorType(XmlAccessType.FIELD)
public class Players implements Serializable {
    
    private static int nextID;
    @XmlElement
    private int playerID;
    @XmlElement
    private String name;
    @XmlElement
    private String club;
    @XmlElement
    private int age;
    @XmlElement
    private String position;
    @XmlElement
    private String nationality;

    public Players() {
        this.playerID=nextID++;
     
    }

   

    public int getPlayerID() {
        return playerID;
    }

    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.playerID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Players other = (Players) obj;
        if (this.playerID != other.playerID) {
            return false;
        }
        return true;
    }
    
    
}
