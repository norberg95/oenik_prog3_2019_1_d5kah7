/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@XmlRootElement(name = "queriedTeams")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueriedTeams implements Serializable {
    @XmlElement(name="team")
    private Teams team;
    

    public void setTeam(Teams team) {
        this.team = team;
    }

    public Teams getTeam() {
        return team;
    }

    
    
}
