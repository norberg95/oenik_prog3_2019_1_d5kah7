/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@XmlRootElement(name ="team")
@XmlAccessorType(XmlAccessType.FIELD)
public class Teams implements Serializable {
    private static int nextID;
    
    private int id;
    @XmlElement
    private String team;
    @XmlElement
    private String manager;
    @XmlElement
    private String captain;
    @XmlElement
    private String kitSponsor;
    @XmlElement
    private List<Players> players=new ArrayList<>();

    public List<Players> getPlayers() {
        return players;
    }
    
    public void setPlayers(List<Players> players) {
        this.players = players;
    }

    public Teams() {
        this.id=nextID++;
    }

   
    

    

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }
    

    public int getId() {
        return id;
    }

    
    

    

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getCaptain() {
        return captain;
    }

    public void setCaptain(String captain) {
        this.captain = captain;
    }

    public String getKitSponsor() {
        return kitSponsor;
    }

    public void setKitSponsor(String kitSponsor) {
        this.kitSponsor = kitSponsor;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Teams other = (Teams) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    
    
    
}
